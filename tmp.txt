This is XeTeX, Version 3.141592653-2.6-0.999993 (TeX Live 2021/Arch Linux) (preloaded format=xelatex)
 \write18 enabled.
entering extended mode
(./ctufit-thesis.tex
LaTeX2e <2021-11-15> patch level 1
L3 programming layer <2022-02-24> (./ctufit-thesis.cls
Document Class: ctufit-thesis 2021/08/06 CTU FIT thesis template
(/usr/share/texmf-dist/tex/latex/base/book.cls
Document Class: book 2021/10/04 v1.4n Standard LaTeX document class
(/usr/share/texmf-dist/tex/latex/base/bk10.clo))
(/usr/share/texmf-dist/tex/generic/babel/babel.sty
(/usr/share/texmf-dist/tex/generic/babel/xebabel.def
(/usr/share/texmf-dist/tex/generic/babel/txtbabel.def))
(/usr/share/texmf-dist/tex/generic/babel-english/english.ldf)
(/usr/share/texmf-dist/tex/generic/babel-czech/czech.ldf))
(/usr/share/texmf-dist/tex/latex/geometry/geometry.sty
(/usr/share/texmf-dist/tex/latex/graphics/keyval.sty)
(/usr/share/texmf-dist/tex/generic/iftex/ifvtex.sty
(/usr/share/texmf-dist/tex/generic/iftex/iftex.sty)))
(/usr/share/texmf-dist/tex/latex/setspace/setspace.sty)
(/usr/share/texmf-dist/tex/latex/xcolor/xcolor.sty
(/usr/share/texmf-dist/tex/latex/graphics-cfg/color.cfg)
(/usr/share/texmf-dist/tex/latex/graphics-def/xetex.def))
(/usr/share/texmf-dist/tex/latex/caption/caption.sty
(/usr/share/texmf-dist/tex/latex/caption/caption3.sty))
(/usr/share/texmf-dist/tex/latex/fancyhdr/fancyhdr.sty)
(/usr/share/texmf-dist/tex/latex/tools/multicol.sty)
(/usr/share/texmf-dist/tex/latex/titlesec/titlesec.sty)
(/usr/share/texmf-dist/tex/latex/amscls/amsthm.sty)
(/usr/share/texmf-dist/tex/latex/mathtools/mathtools.sty
(/usr/share/texmf-dist/tex/latex/tools/calc.sty)
(/usr/share/texmf-dist/tex/latex/mathtools/mhsetup.sty)
(/usr/share/texmf-dist/tex/latex/amsmath/amsmath.sty
For additional information on amsmath, use the `?' option.
(/usr/share/texmf-dist/tex/latex/amsmath/amstext.sty
(/usr/share/texmf-dist/tex/latex/amsmath/amsgen.sty))
(/usr/share/texmf-dist/tex/latex/amsmath/amsbsy.sty)
(/usr/share/texmf-dist/tex/latex/amsmath/amsopn.sty)))
(/usr/share/texmf-dist/tex/latex/amsfonts/amssymb.sty
(/usr/share/texmf-dist/tex/latex/amsfonts/amsfonts.sty)))
(/usr/share/texmf-dist/tex/latex/ellipsis/ellipsis.sty
(/usr/share/texmf-dist/tex/latex/tools/xspace.sty))
(/usr/share/texmf-dist/tex/latex/hyperref/hyperref.sty
(/usr/share/texmf-dist/tex/generic/ltxcmds/ltxcmds.sty)
(/usr/share/texmf-dist/tex/generic/pdftexcmds/pdftexcmds.sty
(/usr/share/texmf-dist/tex/generic/infwarerr/infwarerr.sty))
(/usr/share/texmf-dist/tex/generic/kvsetkeys/kvsetkeys.sty)
(/usr/share/texmf-dist/tex/generic/kvdefinekeys/kvdefinekeys.sty)
(/usr/share/texmf-dist/tex/generic/pdfescape/pdfescape.sty)
(/usr/share/texmf-dist/tex/latex/hycolor/hycolor.sty)
(/usr/share/texmf-dist/tex/latex/letltxmacro/letltxmacro.sty)
(/usr/share/texmf-dist/tex/latex/auxhook/auxhook.sty)
(/usr/share/texmf-dist/tex/latex/kvoptions/kvoptions.sty)
(/usr/share/texmf-dist/tex/latex/hyperref/pd1enc.def)
(/usr/share/texmf-dist/tex/generic/intcalc/intcalc.sty)
(/usr/share/texmf-dist/tex/generic/etexcmds/etexcmds.sty)
(/usr/share/texmf-dist/tex/latex/hyperref/puenc.def)
(/usr/share/texmf-dist/tex/latex/url/url.sty)
(/usr/share/texmf-dist/tex/generic/bitset/bitset.sty
(/usr/share/texmf-dist/tex/generic/bigintcalc/bigintcalc.sty))
(/usr/share/texmf-dist/tex/latex/base/atbegshi-ltx.sty))
(/usr/share/texmf-dist/tex/latex/hyperref/hxetex.def
(/usr/share/texmf-dist/tex/generic/stringenc/stringenc.sty)
(/usr/share/texmf-dist/tex/latex/rerunfilecheck/rerunfilecheck.sty
(/usr/share/texmf-dist/tex/latex/base/atveryend-ltx.sty)
(/usr/share/texmf-dist/tex/generic/uniquecounter/uniquecounter.sty)))
(/usr/share/texmf-dist/tex/latex/pdfpages/pdfpages.sty
(/usr/share/texmf-dist/tex/latex/base/ifthen.sty)
(/usr/share/texmf-dist/tex/latex/eso-pic/eso-pic.sty)
(/usr/share/texmf-dist/tex/latex/graphics/graphicx.sty
(/usr/share/texmf-dist/tex/latex/graphics/graphics.sty
(/usr/share/texmf-dist/tex/latex/graphics/trig.sty)
(/usr/share/texmf-dist/tex/latex/graphics-cfg/graphics.cfg)))
(/usr/share/texmf-dist/tex/latex/pdfpages/ppxetex.def))
(/usr/share/texmf-dist/tex/generic/dirtree/dirtree.sty
(/usr/share/texmf-dist/tex/generic/dirtree/dirtree.tex
`dirtree' v0.32, 2012/12/11 (jcc)))
(/usr/share/texmf-dist/tex/latex/lipsum/lipsum.sty
(/usr/share/texmf-dist/tex/latex/l3packages/l3keys2e/l3keys2e.sty
(/usr/share/texmf-dist/tex/latex/l3kernel/expl3.sty
(/usr/share/texmf-dist/tex/latex/l3backend/l3backend-xetex.def
(|extractbb --version))))
(/usr/share/texmf-dist/tex/latex/lipsum/lipsum.ltd.tex))
(/usr/share/texmf-dist/tex/latex/pgf/frontendlayer/tikz.sty
(/usr/share/texmf-dist/tex/latex/pgf/basiclayer/pgf.sty
(/usr/share/texmf-dist/tex/latex/pgf/utilities/pgfrcs.sty
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgfutil-common.tex
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgfutil-common-lists.tex))
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgfutil-latex.def)
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgfrcs.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/pgf.revision.tex)))
(/usr/share/texmf-dist/tex/latex/pgf/basiclayer/pgfcore.sty
(/usr/share/texmf-dist/tex/latex/pgf/systemlayer/pgfsys.sty
(/usr/share/texmf-dist/tex/generic/pgf/systemlayer/pgfsys.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgfkeys.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgfkeysfiltered.code.tex))
(/usr/share/texmf-dist/tex/generic/pgf/systemlayer/pgf.cfg)
(/usr/share/texmf-dist/tex/generic/pgf/systemlayer/pgfsys-xetex.def
(/usr/share/texmf-dist/tex/generic/pgf/systemlayer/pgfsys-dvipdfmx.def
(/usr/share/texmf-dist/tex/generic/pgf/systemlayer/pgfsys-common-pdf.def))))
(/usr/share/texmf-dist/tex/generic/pgf/systemlayer/pgfsyssoftpath.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/systemlayer/pgfsysprotocol.code.tex))
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcore.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmath.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathcalc.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathutil.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathparser.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.basic.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.trigonometric.code
.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.random.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.comparison.code.te
x) (/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.base.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.round.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.misc.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfunctions.integerarithmetics
.code.tex))) (/usr/share/texmf-dist/tex/generic/pgf/math/pgfmathfloat.code.tex)
) (/usr/share/texmf-dist/tex/generic/pgf/math/pgfint.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorepoints.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorepathconstruct.code.tex
) (/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorepathusage.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorescopes.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcoregraphicstate.code.tex)

(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcoretransformations.code.t
ex) (/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorequick.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcoreobjects.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorepathprocessing.code.te
x) (/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorearrows.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcoreshade.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcoreimage.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcoreexternal.code.tex))
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorelayers.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcoretransparency.code.tex)
 (/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorepatterns.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/basiclayer/pgfcorerdf.code.tex)))
(/usr/share/texmf-dist/tex/generic/pgf/modules/pgfmoduleshapes.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/modules/pgfmoduleplot.code.tex)
(/usr/share/texmf-dist/tex/latex/pgf/compatibility/pgfcomp-version-0-65.sty)
(/usr/share/texmf-dist/tex/latex/pgf/compatibility/pgfcomp-version-1-18.sty))
(/usr/share/texmf-dist/tex/latex/pgf/utilities/pgffor.sty
(/usr/share/texmf-dist/tex/latex/pgf/utilities/pgfkeys.sty
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgfkeys.code.tex))
(/usr/share/texmf-dist/tex/latex/pgf/math/pgfmath.sty
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmath.code.tex))
(/usr/share/texmf-dist/tex/generic/pgf/utilities/pgffor.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/math/pgfmath.code.tex)))
(/usr/share/texmf-dist/tex/generic/pgf/frontendlayer/tikz/tikz.code.tex
(/usr/share/texmf-dist/tex/generic/pgf/libraries/pgflibraryplothandlers.code.te
x) (/usr/share/texmf-dist/tex/generic/pgf/modules/pgfmodulematrix.code.tex)
(/usr/share/texmf-dist/tex/generic/pgf/frontendlayer/tikz/libraries/tikzlibrary
topaths.code.tex))) (/usr/share/texmf-dist/tex/latex/csquotes/csquotes.sty
(/usr/share/texmf-dist/tex/latex/etoolbox/etoolbox.sty)
(/usr/share/texmf-dist/tex/latex/csquotes/csquotes.def)
(/usr/share/texmf-dist/tex/latex/csquotes/csquotes.cfg))
(/usr/share/texmf-dist/tex/latex/biblatex/biblatex.sty
(/usr/share/texmf-dist/tex/latex/logreq/logreq.sty
(/usr/share/texmf-dist/tex/latex/logreq/logreq.def))
(/usr/share/texmf-dist/tex/latex/biblatex/blx-dm.def)
(/usr/share/texmf-dist/tex/latex/biblatex-iso690/iso-numeric.dbx)
(/usr/share/texmf-dist/tex/latex/biblatex/blx-unicode.def)
(/usr/share/texmf-dist/tex/latex/biblatex/blx-compat.def)
(/usr/share/texmf-dist/tex/latex/biblatex/biblatex.def)
(/usr/share/texmf-dist/tex/latex/biblatex-iso690/iso-numeric.bbx
(/usr/share/texmf-dist/tex/latex/biblatex-iso690/iso.bbx))
(/usr/share/texmf-dist/tex/latex/biblatex-iso690/iso-numeric.cbx
(/usr/share/texmf-dist/tex/latex/biblatex/cbx/numeric.cbx)
(/usr/share/texmf-dist/tex/latex/biblatex-iso690/iso-fullcite.cbx))
(/usr/share/texmf-dist/tex/latex/biblatex/biblatex.cfg)
(/usr/share/texmf-dist/tex/latex/biblatex/blx-case-expl3.sty
(/usr/share/texmf-dist/tex/latex/l3packages/xparse/xparse.sty)))
(/usr/share/texmf-dist/tex/latex/minted/minted.sty
(/usr/share/texmf-dist/tex/latex/fvextra/fvextra.sty
(/usr/share/texmf-dist/tex/latex/fancyvrb/fancyvrb.sty)
(/usr/share/texmf-dist/tex/latex/upquote/upquote.sty
(/usr/share/texmf-dist/tex/latex/base/textcomp.sty))
(/usr/share/texmf-dist/tex/latex/lineno/lineno.sty

LaTeX Warning: Command \@parboxrestore  has changed.
               Check if current package is valid.

)

Package fvextra Warning: csquotes should be loaded after fvextra, to avoid a wa
rning from the lineno package on input line 37.

) (/usr/share/texmf-dist/tex/latex/tools/shellesc.sty)
(/usr/share/texmf-dist/tex/latex/ifplatform/ifplatform.sty
(/usr/share/texmf-dist/tex/generic/catchfile/catchfile.sty)
(/usr/share/texmf-dist/tex/generic/iftex/ifluatex.sty) (./ctufit-thesis.w18))
(/usr/share/texmf-dist/tex/generic/xstring/xstring.sty
(/usr/share/texmf-dist/tex/generic/xstring/xstring.tex))
(/usr/share/texmf-dist/tex/latex/framed/framed.sty)
(/usr/share/texmf-dist/tex/latex/float/float.sty))/usr/bin/pygmentize
 (./ctufit-thesis.aux
(./text/text.aux) (./text/appendix.aux) (./text/medium.aux))
(/usr/share/texmf-dist/tex/latex/base/ts1cmr.fd)
*geometry* driver: auto-detecting
*geometry* detected driver: xetex
(/usr/share/texmf-dist/tex/latex/hyperref/nameref.sty
(/usr/share/texmf-dist/tex/latex/refcount/refcount.sty)
(/usr/share/texmf-dist/tex/generic/gettitlestring/gettitlestring.sty))
(./ctufit-thesis.out) (./ctufit-thesis.out)
(/usr/share/texmf-dist/tex/latex/pdflscape/pdflscape.sty
(/usr/share/texmf-dist/tex/latex/graphics/lscape.sty))
(/usr/share/texmf-dist/tex/latex/biblatex-iso690/czech-iso.lbx
(/usr/share/texmf-dist/tex/latex/biblatex/lbx/czech.lbx)
(/usr/share/texmf-dist/tex/latex/biblatex/lbx/czech.lbx))
(/usr/share/texmf-dist/tex/latex/biblatex-iso690/english-iso.lbx
(/usr/share/texmf-dist/tex/latex/biblatex/lbx/english.lbx)
(/usr/share/texmf-dist/tex/latex/biblatex/lbx/english.lbx))
No file ctufit-thesis.bbl.
[1] [2] [1] [2] (./ctufit-thesis.toc
(/usr/share/texmf-dist/tex/latex/amsfonts/umsa.fd)
(/usr/share/texmf-dist/tex/latex/amsfonts/umsb.fd) [3]) [4]
(./ctufit-thesis.lof) (./ctufit-thesis.lot) (./ctufit-thesis.lol) [5] [6]
[7] [8] [9] [10] (./text/text.tex
Kapitola 1.
[1]

Package fancyhdr Warning: \headheight is too small (12.0pt): 
(fancyhdr)                Make it at least 12.69003pt, for example:
(fancyhdr)                \setlength{\headheight}{12.69003pt}.
(fancyhdr)                You might also make \topmargin smaller to compensate:

(fancyhdr)                \addtolength{\topmargin}{-0.69003pt}.

[2]
Kapitola 2.
[3] [4] [5] [6]
Kapitola 3.
[7] [8]
Kapitola 4.
(./_minted-ctufit-thesis/bw.pygstyle)
(./_minted-ctufit-thesis/F78FFCB5C1E00AC6DC34B14F9971BF1B05E43BCBB0EA0CF84F2F78
A9AE65F449.pygtex)
(./_minted-ctufit-thesis/F78FFCB5C1E00AC6DC34B14F9971BF1B05E43BCBB0EA0CF84F2F78
A9AE65F449.pygtex)
(./_minted-ctufit-thesis/7AA27F6C19BEB76CA5AE988B57D3E6B005E43BCBB0EA0CF84F2F78
A9AE65F449.pygtex) [9]
(./_minted-ctufit-thesis/73E8131FCAD48424751786C4B3F6F2FB05E43BCBB0EA0CF84F2F78
A9AE65F449.pygtex)
(./_minted-ctufit-thesis/4404D59BF0E4A0E831791B552782141805E43BCBB0EA0CF84F2F78
A9AE65F449.pygtex) [10] [11] [12]
Kapitola 5.
) [13] (./text/appendix.tex [14]
P\v {r}\'{\i }loha A.
) [15] [16]

LaTeX Warning: Empty bibliography on input line 207.

(./text/medium.tex) [17] (./ctufit-thesis.aux (./text/text.aux)
(./text/appendix.aux) (./text/medium.aux)) )
(see the transcript file for additional information)
Output written on ctufit-thesis.pdf (29 pages).
Transcript written on ctufit-thesis.log.
